/**
	@author natey59
*/

package com.dgpvp.uhc.lobby;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.dgpvp.uhc.lobby.commands._AddPremiumPlayer;
import com.dgpvp.uhc.lobby.commands._CreateGameSign;
import com.dgpvp.uhc.lobby.data.Lobby;
import com.dgpvp.uhc.lobby.listeners.OnPlayerJoin;
import com.dgpvp.uhc.lobby.listeners.OnSignClick;

public class UhcLobby extends JavaPlugin {
	
	private File lobbySave;
		
	private static List<Lobby> lobbies = new ArrayList<Lobby>();
	
	public static HashMap<Player, Integer> shitList = new HashMap<Player, Integer>();
	
	@Override
	public void onEnable() {
		
		PluginManager pm = this.getServer().getPluginManager();
		
		pm.registerEvents(new OnSignClick(), this);
		pm.registerEvents(new OnPlayerJoin(this), this);
		
		this.getCommand("CreateLobby").setExecutor(new _CreateGameSign(this));
		this.getCommand("addpremium").setExecutor(new _AddPremiumPlayer());

	}
	
	public static void addLobby(Lobby lobby) {
		lobbies.add(lobby);
	}
	
	public static List<Lobby> getLobbys() {
		return lobbies;
	}
	
	public void saveLobbys() {
		
		try {
			
			this.lobbySave.createNewFile();
			
			ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(lobbySave));
			
			outputStream.writeObject(lobbies);
			
			outputStream.flush();
			
			outputStream.close();
			
		} catch (IOException e) {
			
			this.getLogger().log(Level.SEVERE, "Failed to save lobby data. There will be no lobby data when the server restarts.");
			
			e.printStackTrace();
		}
		
	}

	
}
