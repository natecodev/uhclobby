/**
	@author natey59
*/

package com.dgpvp.uhc.lobby.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.dgpvp.natey59.uhc.UHC;
import com.dgpvp.natey59.uhc.messenger.PlayerMessenger;
import com.dgpvp.natey59.uhc.player.UHCPlayer;
import com.dgpvp.natey59.uhc.sql.PlayerData;
import com.dgpvp.uhc.lobby.permissions.Permissions;

public class _AddPremiumPlayer implements CommandExecutor {	
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!sender.hasPermission(Permissions.admin)) {
			sender.sendMessage(ChatColor.RED + "You don't have permission to use that command.");
			return true;
		}
		
		PlayerData pData = new PlayerData((UHC)Bukkit.getPluginManager().getPlugin("UHCCore"), new UHCPlayer(Bukkit.getPlayer(args[0])));
		
		pData.setPremium(true);
		
		PlayerMessenger.resultMessage(Bukkit.getPlayer(args[0]), "You are a now a premium (1) player!");
		
		return true;
	}

	
	
}
