/**
	@author natey59
*/

package com.dgpvp.uhc.lobby.commands;

import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.uhc.messenger.PlayerMessenger;
import com.dgpvp.uhc.lobby.UhcLobby;
import com.dgpvp.uhc.lobby.data.Lobby;

public class _CreateGameSign implements CommandExecutor {
	
	UhcLobby lobby;
	
	public _CreateGameSign(UhcLobby lobby) {
		this.lobby = lobby;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		// /uhc sign lobbyName
		
				if (args.length < 2) {
					PlayerMessenger.errorMessage((Player)sender, cmd.getUsage());
					return true;
				}
					
				@SuppressWarnings("deprecation")
				Sign sign = (Sign) ((Player)sender).getTargetBlock(null, 200).getState();
						
				Lobby lobby = new Lobby(args[0], sign);
				
				sign.setLine(0, ChatColor.GREEN + lobby.getName());
								
				UhcLobby.addLobby(lobby);
				
				PlayerMessenger.resultMessage( ((Player)sender), "A lobby sign has been created.");
				
				return true;
		
	}

	
	
}
