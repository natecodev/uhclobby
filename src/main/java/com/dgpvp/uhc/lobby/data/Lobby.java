/**
	@author natey59
*/

package com.dgpvp.uhc.lobby.data;

import java.text.MessageFormat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.uhc.UHC;
import com.dgpvp.natey59.uhc.bungeecord.Connector;
import com.dgpvp.natey59.uhc.sql.ServerData;

public class Lobby {

	private String name;
	private int maxPlayers;
	private int minPlayers;
	private boolean premium;
	
	private LobbyCache cache;
	
	private Sign sign;
	
	@SuppressWarnings("deprecation")
	public Lobby(String name, Sign sign) {
		this.name = name;
		this.sign = sign;
		
		ServerData data = new ServerData((UHC) Bukkit.getPluginManager().getPlugin("UHCCore"), name);
		
		this.maxPlayers = data.getMaxPlayers();
		this.minPlayers = data.getMinPlayers();
		
		this.premium = data.getIsPremium();
		
		this.cache = new LobbyCache((UHC) Bukkit.getPluginManager().getPlugin("UHCCore"), this);
		
		Updater updater = new Updater(this);
		
		Bukkit.getScheduler().scheduleAsyncRepeatingTask((UHC) Bukkit.getPluginManager().getPlugin("UHCCore"), updater, 0, 40);
	}
	
	public String getName() {
		return this.name;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public int getMinPlayers() {
		return minPlayers;
	}

	public boolean isPremium() {
		return premium;
	}
	
	public LobbyCache getCache() {
		return this.cache;
	}
	
	public Sign getSign() {
		return this.sign;
	}
	
	public void connectPlayer(Player player) {
		Connector.connectPlayerToServer((UHC) Bukkit.getPluginManager().getPlugin("UHCCore"), player, this.getName());
	}
	
	class Updater extends BukkitRunnable {
		
		private Lobby lobby;
		//private UhcLobby uhcLobby;
		String line1 = "&a{0}";
		String line2 = "&l&0[{0}&0]";
		String line3 = "[{0}{1}]";
		String line4 = "{0}/{1}";
		
		public Updater(Lobby lobby) {
			this.lobby = lobby;
			//uhcLobby = (UhcLobby) Bukkit.getPluginManager().getPlugin("UhcLobby");
		}
		
		@Override
		public void run() {
			
			this.lobby.getCache().update();
			
			this.lobby.getSign().setLine(0, MessageFormat.format(this.line1, lobby.getName())); //Line one: Lobby Name
			
			this.lobby.getSign().setLine(1, "[" + ((lobby.isPremium())?ChatColor.RED+"Premium":ChatColor.GREEN+"F2P") + ChatColor.BLACK +  "]"); //Line two: premium/f2p
			
			this.lobby.getSign().setLine(2, MessageFormat.format(this.line3, 
					new Object[]{lobby.getCache().getGameState().getColor(), lobby.getCache().getGameState().getReadableName()}));
			
			this.lobby.getSign().setLine(3, MessageFormat.format(line4, new Object[] {lobby.getCache().getPlayerCount(), lobby.getMinPlayers()}));
			
			this.lobby.getSign().update();
		}
				
	}
}
















