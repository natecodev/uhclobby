/**
	@author natey59
*/

package com.dgpvp.uhc.lobby.data;

import com.dgpvp.natey59.uhc.UHC;
import com.dgpvp.natey59.uhc.sql.ServerData;
import com.dgpvp.natey59.uhc.status.GameState;

public class LobbyCache {

	private GameState state;
	
	private ServerData data;
	private int playerCount;
	
	public LobbyCache(UHC core, Lobby lobby) {
		this.data = new ServerData(core, lobby.getName());
	}
	
	public GameState getGameState() {
		return this.state;
	}
	
	public int getPlayerCount() {
		return this.playerCount;
	}
	
	public void update() {
		
		this.state = data.getServerState();
		
		this.playerCount = data.getPlayerCount();
	}
	
}
