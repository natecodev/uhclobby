/**
	@author natey59
*/

package com.dgpvp.uhc.lobby.data;

import java.io.Serializable;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public class SerializedLocation implements Serializable {
	
	private static final long serialVersionUID = 3333175062576350904L;
	
	private int x;
	private int y;
	private int z;
	private String world;
	
	public SerializedLocation(int x, int y, int z, World world) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.world = world.getName();
	}
	
	public Location getLocation() {
		return new Location(Bukkit.getWorld(world), this.x, this.y, this.z);
	}
	
	public static SerializedLocation convert(Location location) {
		return new SerializedLocation(location.getBlockX(), location.getBlockY(), location.getBlockZ(), location.getWorld());
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public int getZ() {
		return this.z;
	}
	
	public World getWorld() {
		return Bukkit.getWorld(world);
	}
	
}
