/**
	@author natey59
*/

package com.dgpvp.uhc.lobby.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.dgpvp.natey59.uhc.UHC;
import com.dgpvp.natey59.uhc.player.UHCPlayer;
import com.dgpvp.natey59.uhc.sql.PlayerData;
import com.dgpvp.uhc.lobby.UhcLobby;

public class OnPlayerJoin implements Listener {

	private UHC uhcCore;
	
	public OnPlayerJoin(UhcLobby uhcLobby) {
		uhcCore = (UHC) uhcLobby.getServer().getPluginManager().getPlugin("UHCCore");
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		
		if (!event.getPlayer().hasPlayedBefore()) {
			
			UHCPlayer newPlayer = new UHCPlayer(event.getPlayer());
			PlayerData pData = new PlayerData(uhcCore, newPlayer);
			
			pData.createPlayerInDatabase();
						
		}
		
	}
	
}