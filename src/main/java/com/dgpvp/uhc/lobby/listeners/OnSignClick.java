/**
	@author natey59
*/

package com.dgpvp.uhc.lobby.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.uhc.lobby.UhcLobby;
import com.dgpvp.uhc.lobby.data.Lobby;

public class OnSignClick implements Listener {

	@SuppressWarnings("deprecation")
	public OnSignClick() {
		
		BukkitRunnable shitListCleaner = new ShitListCleaner();
		
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(Bukkit.getPluginManager().getPlugin("UHCLobby"), shitListCleaner, 0, 10);
		
	}
	
	@EventHandler
	public void onSignClick(PlayerInteractEvent event) {
		
		if (UhcLobby.shitList.get(event.getPlayer()) != null) {
			
			int clickNum = UhcLobby.shitList.get(event.getPlayer()) + 1;
			
			UhcLobby.shitList.remove(event.getPlayer());
			
			UhcLobby.shitList.put(event.getPlayer(), clickNum);
			
			if (clickNum >= 3) {
				event.getPlayer().kickPlayer("Sign spam.");
				return;
			}
			
		} else {
			UhcLobby.shitList.put(event.getPlayer(), 1);
		}
		
		try {
			if (event.getClickedBlock().getType() == Material.SIGN || event.getClickedBlock().getType() == Material.SIGN_POST || event.getClickedBlock().getType() == Material.WALL_SIGN) {
				
				Location location = event.getClickedBlock().getLocation();
				
				for (Lobby lobby : UhcLobby.getLobbys()) {
									
					if (lobby.getSign().getLocation().equals(location)) {
						lobby.connectPlayer(event.getPlayer());
					}
					
				}
				
			}
			
			
		} catch (Exception e) {
			//Bad idea.
		}
		
	}
	
	class ShitListCleaner extends BukkitRunnable {

		@Override
		public void run() {
			
			UhcLobby.shitList.clear();
			
		}
		
		
		
	}
	
}
