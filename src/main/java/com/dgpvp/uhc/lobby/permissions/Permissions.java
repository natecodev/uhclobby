/**
	@author natey59
*/

package com.dgpvp.uhc.lobby.permissions;

import org.bukkit.permissions.Permission;

public class Permissions {

	public static Permission maxPlayerOverride = new Permission("uhc.lobby.maxoverride");
	public static Permission createLobby = new Permission("uhc.lobby.create");
	public static Permission premium = new Permission("uhc.lobby.premium");
	public static Permission admin = new Permission("uhc.lobby.admin");
	
}
